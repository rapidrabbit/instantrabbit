#!/usr/bin/python3
from glob import glob
import json
import os
import os.path
import shutil

if not os.path.exists("dist"):
    os.mkdir("dist")

for fn in glob("providers/*.json"):
    data = json.load(open(fn))
    nicename = data["name"]
    uglyname = os.path.splitext(os.path.basename(fn))[0]
    destdir = "dist/" + uglyname
    if not os.path.exists(destdir):
        os.mkdir(destdir)
    permissions = data["permissions"]
    permissions.append("contextMenus")
    manifest = {
        "manifest_version": 2,
        "version": "1.0",
        "background": { "scripts": ["common.js"] },
        "name": "instantrabbit for " + nicename,
        "omnibox": { "keyword": data["keyword"] },
        "permissions": permissions,
        "homepage_url": "https://gitlab.com/rapidrabbit/instantrabbit",
    }
    if len(data["search"]) != 2:
        raise ValueError("%s: key 'search' must have length 2." % (fn,))
    if len(data["suggestions"]) != 2:
        raise ValueError("%s: key 'suggestions' must have length 2." % (fn,))
    data_json = {
        "provider": nicename,
        "search": data["search"],
        "suggestions": data["suggestions"],
    }
    with open(destdir + "/manifest.json", "w") as manifest_file:
        json.dump(manifest, manifest_file)
    with open(destdir + "/data.json", "w") as data_file:
        json.dump(data_json, data_file)
    shutil.copy("common.js", destdir + "/common.js")
