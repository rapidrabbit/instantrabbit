if (typeof browser == "undefined") {
    browser = chrome;
}

// TODO: these tries are likely to be very sparse; consider a radix tree? But, at the same time, we're likely to have a 'spine' of populated elements as the user types in the query letter by letter, so the gain probably wouldn't be much.
function Trie() {
    let root = {terminal: false, children: new Map()};

    this.add = string => {
        let node = root;
        for (const ch of string) {
            if (!node.children.has(ch)) {
                const child = {terminal: false, children: new Map()};
                node.children.set(ch, child);
                node = child;
            } else {
                node = node.children.get(ch);
            };
        }
        node.terminal = true;
    };
    this.delete = string => {
        let node = root;
        const parents = [];
        for (const ch of string) {
            parents.unshift([ch, node]);
            if (!node.children.has(ch)) {
                return;
            }
            node = node.children.get(ch);
        }
        node.terminal = false;
        for (const [ch, parent] of parents) {
            if (node.terminal || node.children.size > 0) {
                break;
            }
            parent.children.delete(ch);
            node = parent;
        }
    };
    this.getLongestPrefix = string => {
        let node = root;
        let curPrefix = null;
        let trialPrefix = "";
        for (const ch of string) {
            if (!node.children.has(ch)) {
                break;
            }
            node = node.children.get(ch);
            trialPrefix += ch;
            if (node.terminal) {
                curPrefix = trialPrefix;
            }
        }
        return curPrefix;
    };
};

function SuggestionCache() {
    // Traversed in insertion order, time is strictly ascending.
    const responseCache = new Map();
    const seenQueries = new Trie();

    this.addSuggestions = (query, suggestions) => {
        const time = Date.now();
        responseCache.set(query, {time, suggestions});
        seenQueries.add(query);
        for (const [query, {time: time_}] of responseCache) {
            if (responseCache.size <= 20 && time - time_ < 60000) {
                break;
            }
            responseCache.delete(query);
            seenQueries.delete(query);
        }
    };

    this.suggest = query => {
        const query_ = seenQueries.getLongestPrefix(query);
        if (query_ == null) {
            return [];
        }
        return responseCache.get(query_).suggestions;
    };
};

function buildSuggester({provider, suggestionURL, searchURL}) {
    const getOpensearchSuggestions = query => {
        const headers = new Headers();
        headers.append("Accept", "application/json");
        const url = suggestionURL(query);

        if (query == "") {
            return Promise.resolve([]);
        } else {
            return fetch(url, {headers})
                .then(response => response.json())
                .then(json => Promise.resolve(json[1]));
        };
    };

    const formatSuggestion = suggestion => {
        return {
            content: suggestion,
            description: `${suggestion} (search on ${provider})`,
        };
    };

    const sendQuery = (query, disposition) => {
        const url = searchURL(query);
        switch (disposition) {
        case "newForegroundTab":
            browser.tabs.create({url});
            break;
        case "newBackgroundTab":
            browser.tabs.create({url, active: false});
            break;
        default:
            if (disposition != "currentTab") {
                console.log(`Unknown disposition: ${JSON.stringify(disposition)}. Please file a bug!`);
            }
            browser.tabs.update({url});
        };
    };

    const contextMenu = {
        contexts: ["selection"],
        title: `Search on ${provider}`,
        onclick: info => {
            // See issue 3.
            let modifiers = info.modifiers;
            if (typeof modifiers == "undefined") {
                modifiers = [];
            }
            let disposition = "currentTab";
            if (modifiers.includes("Ctrl")) {
                if (modifiers.includes("Shift")) {
                    disposition = "newForegroundTab";
                } else {
                    disposition = "newBackgroundTab";
                }
            }
            sendQuery(info.selectionText, disposition);
        },
    };

    const cache = new SuggestionCache();
    let currentQuery;
    // See issue #4
    browser.omnibox.setDefaultSuggestion({
        description: `Type to search on ${provider}`,
    });
    browser.omnibox.onInputStarted
        .addListener(() => {
            browser.omnibox.setDefaultSuggestion({
                description: `Type to search on ${provider}`,
            });
        });
    browser.omnibox.onInputChanged
        .addListener((query, addSuggestions) => {
            browser.omnibox.setDefaultSuggestion({
                description: `${query} (search on ${provider})`,
            });
            currentQuery = query;
            const updateSuggestions = () =>
                addSuggestions(cache.suggest(currentQuery)
                    .map(formatSuggestion));
            getOpensearchSuggestions(query)
                .then(suggestions => {
                    cache.addSuggestions(query, suggestions);
                    updateSuggestions();
                });
        });
    browser.omnibox.onInputEntered
        .addListener(sendQuery);
    browser.contextMenus.create(contextMenu);
};

fetch(browser.runtime.getURL("data.json"))
    .then(response => response.json())
    .then(json => {
        const buildURL = ([prefix, suffix]) => query =>
            prefix + encodeURIComponent(query) + suffix;
        buildSuggester({
            provider: json["provider"],
            suggestionURL: buildURL(json["suggestions"]),
            searchURL: buildURL(json["search"]),
        });
    });
