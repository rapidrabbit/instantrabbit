This is an ugly, hackish, some-assembly-required replacement for the
much-beloved InstantFox, which will sadly soon stop working. It offers
keyword searches from the location bar with autocomplete suggestions,
as well as an entry in the context menu to search for the selection.

Due to the design of the omnibox WebExtension API, a single
WebExtension can only offer a single search keyword--so what was
previously one extension offering many keywords must now be many
extensions each offering a single keyword. (Hence the name:
instantrabbit, because you very quickly wind up with a lot of the
little sods...)

On the plus side, it works with Chromium now with no additional
effort, and hopefully it won't break when Firefox 57 becomes a
thing. This model is a *slight* improvement in security: if, somehow,
one of the instantrabbit extensions starts leaking information or is
made to run arbitrary code, it can only see a portion of your
searches, and only has permissions to poke at a few origins.

Bugs & annoyances
=================

There are a couple of things that I can't (reasonably) work around:

* Firefox <= 53 allows WebExtensions to meddle with other
  WebExtensions' requests. If you are running uMatrix, this will cause
  you grief. (Not that I lost several hours trying to debug that or
  anything...) And, if you are using the non-WebExtension version of
  uMatrix (like, say, the one available on addons.mozilla.org...),
  this will always cause you grief.

* You will have to manually shift focus away from the location bar
  after selecting an entry. Blame [this
  bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1374656); Chromium
  is also affected, but I don't have a bug number for it.

* Keywords are baked-in to the individual WebExtensions. If you build
  instantrabbit yourself, changing them is easy, but temporary addon
  installation is annoying. If you want to define your own providers,
  again, you have to install locally, which means building it.

* [This bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1332942)
  means that the first suggestion (which is the 'default' suggestion)
  lags a character behind your typing in Firefox.

[GitLab issues](https://gitlab.com/rapidrabbit/instantrabbit/issues)
for other things. Bug reports welcome!

Building and installing
=======================

Run `./build.py`. The unpacked WebExtensions are built in
subdirectories of `dist/`. Go to `about:debugging` and load them as
temporary add-ons; they won't survive a browser restart (until Firefox
55, anyway), but this is good enough for testing.

Installing without building, and also installing permanently
============================================================

I'm planning to put this up on addons.mozilla.org Soon (tm). Watch
this space.

Adding new search providers
===========================

Create a file with a `.json` extension in `providers/`. Use one of the
existing providers as a base. The format for the `search` and
`suggestion` keys is `[prefix, suffix]`; the query is
URL-component-encoded and jammed in between.

Support
=======

Absolutely no guarantees. This is scratching my itch (InstantFox has
worked its way into my muscle memory); so long as it works for me,
hacking on this is not going to be a high priority use of my time. If
it works for you, great!

With that said, patches are welcome and bug reports are very, very
welcome; I will very likely merge pull requests that add new providers
without a second thought. If your patch is more than a small thing, or
something that might cause maintainability woes, please open an issue
to discuss the design *before* you spend five hundred hours on it.
